const Eris = require('eris');
const fs = require('fs');
const config = require('./config.json');

// Replace BOT_TOKEN with your bot account's token
var bot = new Eris.CommandClient(config.token, {}, {
    description: "A Discord Social Network",
    owner: "Noculi🐾#6636",
    prefix: "om."
});

bot.on("ready", () => { // When the bot is ready
    console.log("Ready!"); // Log "Ready!"
});

bot.registerCommandAlias("whatdo", "help"); // Alias !halp to !help

bot.registerCommand("ping", ["Pang!", "Peng!", "Pong!", "Pung!"], {
    description: "Ping!",
    fullDescription: "Just to check if the bot is actually working!"
});

bot.registerCommand("register", (msg, args) => { 
    if(args.length === 0) {
        return "Enter the username you want.";
    }
    finalText = msg.author.username + '#' + msg.author.discriminator + ' You will be able to register soon! Just hold on!'
    return finalText;
}, {
    description: "Register your ID with Onmioid",
    fullDescription: "The bot will register your Discord ID with Omnioid so you can use it.",
    usage: "<username>"
});

bot.registerCommand("follow", (msg, args) => {
    if(args.length === 0) {
        return "Enter username of a person to follow them.";
    }
    finalText = msg.author.username + '#' + msg.author.discriminator + ' You will be able to follow people soon! Just hold on!'
    return finalText; 
}, {
    description: "Follow a user",
    fullDescription: "Follow another user to get all their posts.",
    usage: "<username>"
});

bot.registerCommand("unfollow", (msg, args) => {
    if(args.length === 0) {
        return "Enter username of a person to unfollow them.";
    }
    finalText = msg.author.username + '#' + msg.author.discriminator + ' You will be able to unfollow people soon! Just hold on!'
    return finalText; 
}, {
    description: "Unfollow a user",
    fullDescription: "Unfollow another user to stop getting all their posts.",
    usage: "<username>"
});

bot.registerCommand("post", (msg, args) => {
    if(args.length === 0) {
        return "Enter message to post.";
    }
    finalText = msg.author.username + '#' + msg.author.discriminator + ' You will be able to post messages soon! Just hold on!'
    return finalText; 
}, {
    description: "Post a message",
    fullDescription: "Post a message to all your followers.",
    usage: "<message>"
});

if (fs.existsSync('config.json')) {
    if(config.token === "ENTER TOKEN HERE"){
        console.log('Please go into your config.json file and place your token under the token field.');
    } else {
        if(config.databaseurl === "ENTER DATABASE URL HERE"){
            console.log('Please go into your config.json file and place your database url under the databaseurl field.');
        } else {
            bot.connect();
        }
    }
} else {
    console.log('Please rename example-config.json to config.json and then fill out the fields.');
}
