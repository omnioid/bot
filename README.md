# Omnioid
### Discord Social Network


Current Features
------
Nothing

To-Do
------
- [ ] Basic Auth and Database
    - [ ] User accounts based on ID
    - [ ] Posting and reciving content
    - [ ] Following and unfollowing users
- [ ] User profiles
- [ ] API?

Starting it up
------
1. Do `npm install` to get all required modules
2. Copy and rename example-config.json to config.json
3. Add token and database url to config.json
4. Do `node index.js`